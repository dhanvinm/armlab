import sys
import cv2
import numpy as np
import time
import math
import ast
from PyQt4 import QtGui, QtCore, Qt
from ui import Ui_MainWindow
from rexarm import Rexarm
from video import Video
from copy import deepcopy
import pickle
from kinematics import *
from operator import itemgetter

""" Radians to/from  Degrees conversions """
D2R = 3.141592/180.0
R2D = 180.0/3.141592

""" Pyxel Positions of image in GUI """
MIN_X = 310
MAX_X = 950

MIN_Y = 30
MAX_Y = 510

# File to dump and retrieve plan
plan_file = 'waypoints/original_plan.pk'
smooth_plan_file = 'waypoints/smooth_plan.pk'

def get_theta(r):
    wrist_range = (-30.0,-120.0)
    r_range = (295.0,15.0)
    great_r = (80.0,200.0)
    if r < great_r[0] and r > great_r[1]:
        return -90.0
    if r < r_range[0] and r > r_range[1]:
        th = wrist_range[1] + (r - r_range[1])* (wrist_range[0] - wrist_range[1])/(r_range[0] - r_range[1])
        print th
        return th
    else:
        return -180.0

# Returns a list of 4 quadruples
# Each quadruple defines the spline for
# the corresponding joint
def get_spline(pos_wp1,vel_wp1,pos_wp2,vel_wp2):
    res = list()
    for i in range(len(pos_wp1)):
        q0 = pos_wp1[i]
        v0 = vel_wp1[i]
        qf = pos_wp2[i]
        vf = vel_wp2[i]

        a0 = q0
        a1 = v0
        a2 = 3.0*(qf - q0) - (2*v0+vf)
        a3 = 2*(q0-qf)+(v0+vf)

        res.append((a0,a1,a2,a3))
    return res

def get_xyzt(x,y):
    r = math.sqrt(x**2+y**2)
    rx = x
    ry = y
    rz = 15
    rt = get_theta(r)
    return rx,ry,rz,rt


def eval_pos_cubic(a0,a1,a2,a3,t):
    return a0+ t*a1+ a2*t*t+ a3*t*t*t

def eval_vel_cubic(a0,a1,a2,a3,t):
    return a1+ 2*a2*t+ 3*a3*t*t

# Assumes ti=0; tf=1.0
def get_splines(pos_wpts,vel_wpts):
    # sample
    nsamp = 10
    dt = 1.0/nsamp
    smooth_pos_wpts = list()
    smooth_vel_wpts = list()
    print "Splines"
    print len(pos_wpts)
    print len(vel_wpts)
    for i in range(1,len(pos_wpts)):
        print i
        print 
        joint_splines = get_spline(pos_wpts[i-1],vel_wpts[i-1],pos_wpts[i],vel_wpts[i])
        t = 0.0
        # Generate samples from the
        while t<=1.0:
            samp_pos = list()
            samp_vel = list()
            # For every joint
            for jidx in range(len(joint_splines)):
                a0 = joint_splines[jidx][0]
                a1 = joint_splines[jidx][1]
                a2 = joint_splines[jidx][2]
                a3 = joint_splines[jidx][3]

                samp_pos.append(eval_pos_cubic(a0,a1,a2,a3,t));
                samp_vel.append(eval_vel_cubic(a0,a1,a2,a3,t));
            smooth_pos_wpts.append(deepcopy(samp_pos))
            smooth_vel_wpts.append(deepcopy(samp_vel))
            t = t+dt
    return smooth_pos_wpts,smooth_vel_wpts

# Get velocities given positions
def get_vels_for_spline(wpt_lst):
    n = len(wpt_lst)
    wpts_2off = np.array(wpt_lst[2:])
    wpts = np.array(wpt_lst[0:n-2])
    mid = 3*(wpts_2off-wpts)
    print mid
    mid = mid.tolist()
    print mid
    top = [3*(wpt_lst[1] - wpt_lst[0])]
    bot = [3*(wpt_lst[n-1] - wpt_lst[n-2])]

    tmp = top.extend(mid)
    fin = top.extend(bot)
    tridiag = 2*np.diag(np.ones(n))
    for i in range(1,n-1):
        tridiag[i,i-1] += 1
        tridiag[i,i] += 2
        tridiag[i,i+1] += 1
    tridiag[0,1] = 1
    tridiag[n-1,n-2] = 1
    print tridiag
    diffs = deepcopy(top)
    vels = np.linalg.inv(np.mat(tridiag))*np.mat(diffs).transpose()
    xx =  vels.transpose()
    yy = xx.tolist()
    print yy[0]
    return yy[0]

def splinify(sm_wpts):
    xx = np.array(sm_wpts)
    xxt = xx.transpose()
    trans_lst = xxt.tolist()
    vels = []
    for l in trans_lst:
        vels.append(get_vels_for_spline(l))
    vv = np.array(vels)
    v = vv.transpose()
    return sm_wpts,v.tolist()



def interpolate(waypoints,sample):
    num_int = sample
    dims = len(waypoints[0])
    npts = len(waypoints)
    extended_wpts = list()
    extended_wpts.append(waypoints[0])
    for i in range(1,npts):
        incr = list()
        for j in xrange(dims):
            incr.append(float(waypoints[i][j] - waypoints[i-1][j])/(num_int-1))
            #print incr
        for idx in range(1,num_int):
            mid_pt = list()
            for jdx in xrange(dims):
                mid_pt.append(waypoints[i-1][jdx]+idx*incr[jdx])
            extended_wpts.append(deepcopy(mid_pt))
    #for tmp in extended_wpts:
    #    print tmp
    return extended_wpts

def filter_path(waypoints,sample):
    all_np = np.transpose(np.array(waypoints))
    lst = list()
    for x in all_np:
        lst.append(np.convolve(x,[1.0/3,1.0/3,1.0/3],'same'))
    final = np.transpose(np.array(lst)).tolist()
    #print len(final)
    result = []
    for i in range(len(final)):
        #print "Iteration", i
        if (i%(sample-1)) == 0:
            result.append(waypoints[i])
        else:
            result.append(final[i])

    #for i in result:
    #    print i

    return result

class Gui(QtGui.QMainWindow):
    """
    Main GUI Class
    It contains the main function and interfaces between
    the GUI and functions
    """
    def __init__(self,parent=None):
        QtGui.QWidget.__init__(self,parent)
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)
        """ Main Variables Using Other Classes"""
        self.rex = Rexarm()
        self.video = Video(cv2.VideoCapture(0))
        """ Other Variables """
        self.last_click = np.float32([0,0])

        """ Set GUI to track mouse """
        QtGui.QWidget.setMouseTracking(self,True)

        """
        Video Function
        Creates a timer and calls play() function
        according to the given time delay (27mm)
        """
        self._timer = QtCore.QTimer(self)
        self._timer.timeout.connect(self.play)
        self._timer.start(27)

        """
        LCM Arm Feedback
        Creates a timer to call LCM handler continuously
        No delay implemented. Reads all time
        """
        self._timer2 = QtCore.QTimer(self)
        self._timer2.timeout.connect(self.rex.get_feedback)
        self._timer2.start()

        """
        Connect Sliders to Function
        TO DO: CONNECT THE OTHER 5 SLIDERS IMPLEMENTED IN THE GUI
        """
        self.ui.sldrBase.valueChanged.connect(self.slider_change)
        self.ui.sldrElbow.valueChanged.connect(self.slider_change)
        self.ui.sldrWrist.valueChanged.connect(self.slider_change)
        self.ui.sldrShoulder.valueChanged.connect(self.slider_change)
        self.ui.sldrSpeed.valueChanged.connect(self.slider_change)
        self.ui.sldrMaxTorque.valueChanged.connect(self.slider_change)

        """ Commands the arm as the arm initialize to 0,0,0,0 angles """
        self.slider_change()

        """ Connect Buttons to Functions """
        self.ui.btnLoadPlan.clicked.connect(self.load_plan)
        self.ui.btnLoadCameraCal.clicked.connect(self.load_camera_cal)
        self.ui.btnPerfAffineCal.clicked.connect(self.affine_cal)
        self.ui.btnTeachRepeat.clicked.connect(self.tr_initialize)
        self.ui.btnAddWaypoint.clicked.connect(self.tr_add_waypoint)
        self.ui.btnSmoothPath.clicked.connect(self.tr_smooth_path)
        self.ui.btnPlayback.clicked.connect(self.tr_playback)
        self.ui.btnDefineTemplate.clicked.connect(self.def_template)
        self.ui.btnLocateTargets.clicked.connect(self.template_match)
        self.ui.btnExecutePath.clicked.connect(self.exec_path)

        self.A = []
        self.b = []
        self.waypoints = []
        self.smooth_waypoints = []
        self.use_smooth = False

        self.kin =  Kinematics()
        self.tr_3_2 = False
        self.tr_3_3 = False


    def play(self):
        """
        Play Funtion
        Continuously called by GUI
        """

        """ Renders the Video Frame """
        try:
            self.video.captureNextFrame()
            self.ui.videoFrame.setPixmap(self.video.convertFrame())
            self.ui.videoFrame.setScaledContents(True)
        except TypeError:
            print "No frame"

        """
        Update GUI Joint Coordinates Labels
        TO DO: include the other slider labels
        """
        self.ui.rdoutBaseJC.setText(str(self.rex.joint_angles_fb[0]*R2D))
        self.ui.rdoutShoulderJC.setText(str(self.rex.joint_angles_fb[1]*R2D))
        self.ui.rdoutElbowJC.setText(str(self.rex.joint_angles_fb[2]*R2D))
        self.ui.rdoutWristJC.setText(str(self.rex.joint_angles_fb[3]*R2D))

        th = deepcopy(self.rex.joint_angles_fb)
        rbt = self.kin.get_fkmat(th[0],th[1],th[2],th[3])
        z_in = np.mat([0,0,1,0]).transpose()
        res_vec = np.array(rbt*z_in)
        dxy = math.sqrt(1 - res_vec[2]*res_vec[2])
        tool_theta = math.atan2(res_vec[2],dxy)
        aa = rbt[0,3]
        bb = rbt[1,3]
        cc = rbt[2,3]
        self.ui.rdoutX.setText(str(aa*1000))
        self.ui.rdoutY.setText(str(bb*1000))
        self.ui.rdoutZ.setText(str(cc*1000))
        self.ui.rdoutT.setText(str(tool_theta*R2D))



        """
        Mouse position presentation in GUI
        TO DO: after getting affine calibration make the apprriate label
        to present the value of mouse position in world coordinates
        """
        x = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).x()
        y = QtGui.QWidget.mapFromGlobal(self,QtGui.QCursor.pos()).y()
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.ui.rdoutMousePixels.setText("(-,-)")
            self.ui.rdoutMouseWorld.setText("(-,-)")
        else:
            x = x - MIN_X
            y = y - MIN_Y
            self.ui.rdoutMousePixels.setText("(%.0f,%.0f)" % (x,y))
            if (self.video.aff_flag == 2):
                """ TO DO Here is where affine calibration must be used """
                tmp = np.matrix.reshape(self.video.aff_matrix,(2,3))*np.mat([[x],[y],[1]])
                self.ui.rdoutMouseWorld.setText("(%.0f,%.0f)" % (tmp[0],tmp[1]))
            else:
                self.ui.rdoutMouseWorld.setText("(-,-)")

        """
        Updates status label when rexarm playback is been executed.
        This will be extended to includ eother appropriate messages
        """
        if(self.rex.plan_status == 1):
            self.ui.rdoutStatus.setText("Playing Back - Waypoint %d"
                                    %(self.rex.wpt_number + 1))


    def slider_change(self):
        """ plan
        Function to change the slider labels when sliders are moved
        and to command the arm to the given position
        TO DO: Implement for the other sliders
        """
        # XXX:Use speed
        if self.rex.plan_status == 0:
            self.ui.rdoutBase.setText(str(self.ui.sldrBase.value()))
            self.ui.rdoutShoulder.setText(str(self.ui.sldrShoulder.value()))
            self.ui.rdoutElbow.setText(str(self.ui.sldrElbow.value()))
            self.ui.rdoutWrist.setText(str(self.ui.sldrWrist.value()))
            self.ui.rdoutTorq.setText(str(self.ui.sldrMaxTorque.value()) + "%")
            self.ui.rdoutSpeed.setText(str(self.ui.sldrSpeed.value()) + "%")
            self.rex.max_torque = self.ui.sldrMaxTorque.value()/100.0
            self.rex.speed = self.ui.sldrSpeed.value()/100.0
            self.rex.joint_angles[0] = self.ui.sldrBase.value()*D2R
            self.rex.joint_angles[1] = self.ui.sldrShoulder.value()*D2R
            self.rex.joint_angles[2] = self.ui.sldrElbow.value()*D2R
            self.rex.joint_angles[3] = self.ui.sldrWrist.value()*D2R
            self.rex.cmd_publish()

    def mousePressEvent(self, QMouseEvent):
        """
        Function used to record mouse click positions for
        affine calibration
        """

        """ Get mouse posiiton """
        x = QMouseEvent.x()
        y = QMouseEvent.y()

        """ From QtEvent to Video Coords"""
        inc_x = 107
        inc_y = 73

        """ If mouse position is not over the camera image ignore """
        if ((x < MIN_X) or (x > MAX_X) or (y < MIN_Y) or (y > MAX_Y)):
            self.video.setting_template_first = False
            self.video.setting_template_unconfirmed = False
            self.video.setting_template_second = False
            self.video.setting_template_confirmed = False
            return
        if (self.video.setting_template_first):
            print "Set First Point"
            self.video.temp_pt1[0] = 2*(x - MIN_X )
            self.video.temp_pt1[1] = 2*(y - MIN_Y )
            print self.video.temp_pt1
            self.video.setting_template_second = True
            self.video.setting_template_first = False

        elif (self.video.setting_template_second):
            print "Set Second Point"
            self.video.temp_pt2[0] = 2*(x - MIN_X )
            self.video.temp_pt2[1] = 2*(y - MIN_Y )
            print self.video.temp_pt2
            self.video.setting_template_second = False
            self.video.setting_template_unconfirmed = True
        elif (self.video.setting_template_unconfirmed):
            print "Confirmed"
            self.video.setting_template_unconfirmed = False
            self.video.setting_template_confirmed = True

        """ Change coordinates to image axis """
        self.last_click[0] = x - MIN_X
        self.last_click[1] = y - MIN_Y


        """ If affine calibration is been performed """
        if (self.video.aff_flag == 1):
            """ Save last mouse coordinate """
            self.video.mouse_coord[self.video.mouse_click_id] = [(x-MIN_X),
                                                                 (y-MIN_Y)]

            """ Update the number of used poitns for calibration """
            self.video.mouse_click_id += 1

            """ Update status label text """
            self.ui.rdoutStatus.setText("Affine Calibration: Click point %d"
                                      %(self.video.mouse_click_id + 1))

            """
            If the number of click is equal to the expected number of points
            computes the affine calibration.
            TO DO: Change this code to use you programmed affine calibration
            and NOT openCV pre-programmed function as it is done now.
            """
            if(self.video.mouse_click_id == self.video.aff_npoints):
                """
                Update status of calibration flag and number of mouse
                clicks
                """
                self.video.aff_flag = 2
                self.video.mouse_click_id = 0

                """ Perform affine calibration with OpenCV """
                # self.video.aff_matrix = cv2.getAffineTransform(
                #                         self.video.mouse_coord,
                #                         self.video.real_coord)
                row_val = 0
                for point in self.video.mouse_coord:
                    self.A.append([point[0] , point[1], 1 , 0 , 0 , 0])
                    self.b.append([self.video.real_coord[row_val][0]])
                    self.A.append([0, 0, 0, point[0] , point[1], 1])
                    self.b.append([self.video.real_coord[row_val][1]])
                    row_val += 1
                print "\nA\n"
                print np.array(self.A)
                print "\nB\n"
                print np.array(self.b)
                self.video.aff_matrix = np.mat(np.linalg.pinv(self.A)) * np.mat(self.b)
                #print np.linalg.lstsq(np.array(self.A),np.array(self.b))
                #self.aff_matrix = np.linalg.lstsq(np.array(self.A),np.array(self.b))[0]
                """ Updates Status Label to inform calibration is done """
                self.ui.rdoutStatus.setText("Waiting for input")

                """
                Uncomment to gether affine calibration matrix numbers
                on terminal
                """
                print "\nCoeffs\n"
                print self.video.aff_matrix
                #print type(self.video.aff_matrix)
                #print np.matrix.reshape(self.video.aff_matrix,(2,3))
                #row_val = 0
                #for point in self.video.mouse_coord:
                #    xx = np.mat([[point[0]],[point[1]],[1.0]])
                #    print "\n Debug\n"
                #    print xx
                #    print np.mat(np.matrix.reshape(self.video.aff_matrix,(2,3)))*xx
                #    row_val +=1

    def affine_cal(self):
        """
        Function called when affine calibration button is called.
        Note it only chnage the flag to record the next mouse clicks
        and updates the status text label
        """
        self.video.aff_flag = 1
        self.ui.rdoutStatus.setText("Affine Calibration: Click point %d"
                                    %(self.video.mouse_click_id + 1))

    def load_camera_cal(self):
        self.video.camera_matrix = np.load('calib_cam_mat.npy')
        self.video.dist_coefs = np.load('calib_dist.npy')
        w = int(self.video.capture.get(3))
        h = int(self.video.capture.get(4))
        self.video.new_camera_matrix,self.video.roi = cv2.getOptimalNewCameraMatrix(self.video.camera_matrix,self.video.dist_coefs,(w,h),1,(w,h))
        self.video.calibrated = True
        print "Load Camera Cal"

    def tr_initialize(self):
        self.rex.plan_status = 1
        self.waypoints = []
        self.smooth_waypoints = []
        print "Teach and Repeat"
        print "Initial point",self.rex.joint_angles_fb
        point = self.rex.joint_angles_fb
        self.waypoints.append([point[0],point[1],point[2],point[3]])
        self.rex.speed = 0.0
        self.rex.max_torque = 0.0
        self.rex.cmd_publish()

    def tr_add_waypoint(self):
        print "Add Waypoint"
        print "Prev waypoints",self.waypoints
        point = self.rex.joint_angles_fb
        self.waypoints.append([point[0],point[1],point[2],point[3]])
        print "Number of waypoints = ",len(self.waypoints)
        # print map(np.degrees,self.waypoints)

    def tr_smooth_path(self):
        read_waypoints = False

        if read_waypoints:
            waypoint_file = open('waypoints.txt','r')
            lists_of_waypoints = waypoint_file.readline()
            waypoint_file.close()
            self.waypoints = ast.literal_eval(lists_of_waypoints)

        #velocities = list()
        #for pt in self.waypoints:
        #    xx = list()
        #    for j in pt:
        #        xx.append(0.01)
        #    velocities.append(deepcopy(xx))
        #[spl_pos,spl_vel] = get_splines(self.waypoints,velocities)
        #print "\nSpline Point\n--------------\n"
        #print spl_pos

        #print "\nSpline velocities\n--------------\n"
        #print spl_vel


        #print "Length = ",len(spl_pos)
        # Save plan
        #with open(plan_file,'w') as f:
        #    pickle.dump((spl_pos,spl_vel),f)
        

        # Interpolating code
        self.use_smooth = True
        self.smooth_waypoints = []
        wp0 = deepcopy(self.rex.joint_angles_fb)
        tmp = [wp0]
        for i in self.waypoints:
            if self.tr_3_2:
                self.smooth_waypoints.append([i[0],(i[1]+ 20*D2R),i[2],i[3]])
                self.smooth_waypoints.append(i)
                self.smooth_waypoints.append([i[0],(i[1]+ 20*D2R),i[2],i[3]])
            elif self.tr_3_3:
                tmp.append([i[0],(i[1]+ 20*D2R),i[2],i[3]])
                # tmp.append([i[0],(i[1]+ 10*D2R),i[2],i[3]])
                tmp.append(i)
                # tmp.append([i[0],(i[1]+ 10*D2R),i[2],i[3]])
                tmp.append([i[0],(i[1]+ 20*D2R),i[2],i[3]])
            else:
                tmp.append(i)

        
        print "Waypoints"
        for xx in tmp:
            print xx
        print "-------------------------\n\n"
        no_of_samples = 3
        #lst = interpolate(self.waypoints,no_of_samples)
        lst = interpolate(tmp,no_of_samples)
        self.smooth_waypoints = filter_path(lst,no_of_samples)
        self.smooth_waypoints = lst
        print "Size of smooth: ",len(self.smooth_waypoints)
        print "Smoothed Waypoints: \n----------------\n"
        for xxx in self.smooth_waypoints:
            print xxx
        print "-------------------------\n\n"
        # Uncomment for spline 
        pts,vels = splinify(self.smooth_waypoints)
        self.smooth_waypoints,zzz =  get_splines(pts,vels)
        
        # Save smooth plan
        with open(smooth_plan_file,'w') as f:
            pickle.dump(self.smooth_waypoints,f)

    def tr_playback(self):
        print "Playback"
        # Save plan
        with open(plan_file,'w') as f:
            pickle.dump(self.waypoints,f)
        # Save smooth plan
        with open(smooth_plan_file,'w') as f:
            pickle.dump(self.smooth_waypoints,f)
        
        self.rex.wpt_number = 0

        if self.use_smooth:
            print "Playing Back SMOOOOTH"
            self.rex.plan = self.smooth_waypoints
            self.rex.wpt_total = len(self.smooth_waypoints)
            self.use_smooth = False
        else:
            print "Not SMOOOOTH"
            self.rex.plan = self.waypoints
            self.rex.wpt_total = len(self.waypoints)

        print "Plan",self.waypoints
        self.rex.plan_command()
        print "Returned from plan_command"
        self.rex.plan_status = 0

    def def_template(self):
        print "Define Template"
        self.video.setting_template_first = True
        self.video.template_ready = True

    def template_match(self):
        print "Template Match"
        if(self.video.template_ready):
            #tmp = deepcopy(self.video.currentFrame)
            #self.video.tm.img  = cv2.cvtColor(tmp, cv2.COLOR_BGR2GRAY)
            a = int(round(time.time()))
            res = self.video.tm.cv_template_match()
            b = int(round(time.time()))
            print "Time : ",(b - a)
            final = []
            xx = []
            for pt in res:
                print pt
                print self.video.match_offset
                x = pt[0]/2 + self.video.match_offset[0]/4
                y = pt[1]/2 + self.video.match_offset[1]/4
                #x = pt[0]/2
                #y = pt[1]/2
                print (x,y)
                final.append([x,y])
            # Move
            self.video.endpoints = deepcopy(final)
            self.video.endpoints_set = True

            endpoints = []
            for loc in final:
                world_xy =   np.matrix.reshape(self.video.aff_matrix,(2,3))*np.mat([[loc[0]],[loc[1]],[1]])
                x = world_xy[0].tolist()
                y = world_xy[1].tolist()
                endpoints.append([x[0][0],y[0][0]])
            for endpt in endpoints:
                self.endpoints = deepcopy(endpoints)
            print "WORLD TEMPLATE MATCHES\n--------------\n"
            print endpoints
            print"-----------------"
            # TODO: Sort according to theta,r in 2D plane.


            self.waypoints = []
            for ep in endpoints:
                x,y,z,t = get_xyzt(ep[0],ep[1])
                cand = []
                angles = self.kin.get_invkin(x,y,z,t,0)
                print angles
                if angles and collision_check(angles):
                    cand.append(angles)
                angles = self.kin.get_invkin(x,y,z,t,1)
                print angles
                if angles and collision_check(angles):
                    cand.append(angles)
                # TODO: Sort according to theta,r in 2D plane.
                chosen = []
                if(len(cand)>0):
                    chosen = cand[0]

                if(len(cand)>1):
                    pass
                if(chosen):
                    self.waypoints.append(map(lambda x:x*D2R,chosen))
                else:
                    "\n\nFailed :(\n===============\n\n"
                print cand
            print "DEBUG - UNSORTED\n"
            print self.waypoints
            print "DEBUG - SORTED\n"
            self.waypoints = sorted(self.waypoints, key=itemgetter(0))
            print self.waypoints


    def load_plan(self):
        f = open(plan_file,'rb')
        self.rex.plan = pickle.load(f)
        f.close()

    def exec_path(self):
        print "Execute Path"
        #self.rex.use_custom_vel = True
        print self.rex.plan
        self.rex.plan_command()
        #self.rex.use_custom_vel = False

def main():
    app = QtGui.QApplication(sys.argv)
    ex = Gui()
    ex.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()
