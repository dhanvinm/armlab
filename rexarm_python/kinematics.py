import sys
import numpy as np
import math
D2R = 3.141592/180.0

min_angles = map(math.radians,[-180,-118,-118,-120])
max_angles = map(math.radians,[180,120,116,125])
        
def collision_check(q):
    """
    Perform a collision check with the ground and the base
    takes a 4-tuple of joint angles q
    returns true if no collision occurs
    """
    angles = map(lambda x:x*D2R,q)
    if(angles[0]<min_angles[0] or angles[0]>max_angles[0]):
        return False
    if(angles[1]<min_angles[1] or angles[1]>max_angles[1]):
        return False
    if(angles[2]<min_angles[2] or angles[2]>max_angles[2]):
        return False
    if(angles[3]<min_angles[3] or angles[3]>max_angles[3]):
        return False
    return True 


class Rot():
    "Rotation Class -  Assumes ||w|| = 1"
    def __init__(self,w):
        self.w = np.array(w)
        self.w_hat = np.mat([[0,-w[2],w[1]],\
                      [w[2],0,-w[0]],\
                      [-w[1],w[0],0]])

    def get_exp(self,theta):
        return np.eye(3) + self.w_hat*math.sin(theta) + self.w_hat*self.w_hat*(1-math.cos(theta))

class Twist():
    "Twist Class"
    def __init__(self,w,q):
        self.v = -np.cross(np.array(w),np.array(q))
        self.rot = Rot(w)
        # Cache for efficiency
        self.wwtv = np.mat(self.rot.w).transpose()*np.mat(self.rot.w)* np.mat(self.v).transpose()
        self.wxv = np.mat(np.cross(self.rot.w,self.v)).transpose()
    def get_exp(self,theta):
        ul = self.rot.get_exp(theta)
        tmp = np.eye(3) - ul
        ur = tmp*self.wxv + self.wwtv*theta
        um = np.concatenate((ul,ur),axis=1)
        return np.vstack((um,np.array([0,0,0,1])))

def sq(x):
    return x*x

class Kinematics():
    """
    The Kinematics class that contains all the transformations for 
    forward and backward kinematics for the rexarm

    Forward Kin - Used POX. All measurements in meters in intertial frame
    """

    def __init__(self):
        # Axes of rotations for
        # Base (wb), shoulder(ws), elbow (we), wrist(ww)
        self.wb = [0.0,0.0,1.0]
        self.ws = [1.0,0.0,0.0]
        self.we = [1.0,0.0,0.0]
        self.ww = [1.0,0.0,0.0]

        # Axes points in inertial frame
        self.qb = [0.0,0.0,0.0]
        self.qs = [0.0,0.0,0.12]
        self.qe = [0.0,0.0,0.22]
        self.qw = [0.0,0.0,0.32]

        # dimensions (mm)
        self.bs = 118.0 # base to shoulder
        self.se = 102.0 # shoulder to elbow
        self.ew = 102.0 # elbow to wrist
        self.wt = 110.0
        self.transform0 = np.mat([[1,0,0,0],\
                                  [0,1,0,0],\
                                  [0,0,1,0.428],\
                                  [0,0,0,1]])
        self.tb = Twist(self.wb,self.qb)
        self.ts = Twist(self.ws,self.qs)
        self.te = Twist(self.we,self.qe)
        self.tw = Twist(self.ww,self.qw)

    def get_fkmat(self,thb,ths,the,thw):
        return self.tb.get_exp(thb)\
                *self.ts.get_exp(ths)\
                *self.te.get_exp(the)\
                *self.tw.get_exp(thw)\
                *self.transform0

    # Assumes th in degrees
    def get_invkin(self,x,y,z,th,conf):
        base = math.atan2(x,y)      # in radians
        #print "Base Angle = {0}".format(base)
        # We go into the plane of the arm
        print "Destination = ({0},{1},{2},{3})".format(x,y,z,th)
        # Origin is at shoulder joint
        tip_xnew = math.sqrt(x*x+y*y)
        tip_ynew = z - self.bs
        print "Tip = ({0},{1})".format(tip_xnew,tip_ynew)
        wrist_xnew = tip_xnew - self.wt*math.cos(th*D2R)
        wrist_ynew = tip_ynew - self.wt*math.sin(th*D2R)
        l1 = self.se
        l2 = self.ew
        r = math.sqrt(sq(wrist_xnew)+sq(wrist_ynew))
        print "Wrist = ({0},{1})".format(wrist_xnew,wrist_ynew)
        #print "R,l1,l2 = {0},{1},{2}".format(r,l1,l2)
        # angle between l1 and l2 (in radians) [0,pi]
        cos_alpha = (sq(l1)+sq(l2)-sq(r))/(2*l1*l2)
        #print cos_alpha
        #print "Hi"
        if cos_alpha <-1 or cos_alpha>1:
            # We are out of the workspace
            return []
        #print cos_alpha
        alpha = math.acos((sq(r) - sq(l1) - sq(l2))/(-2*l1*l2))
        #print alpha
        #print alpha/D2R
        abs_th_elb = math.pi - alpha
        big_th = math.atan2(wrist_ynew,wrist_xnew)
        #print "Big Theta = {0}".format(big_th/D2R)
        if conf == 1:
            shoulder = math.pi/2 - big_th - abs_th_elb/2
            elbow = abs_th_elb
        else:
            shoulder =  math.pi/2 - big_th + abs_th_elb/2
            elbow = - abs_th_elb
        wrist =  math.pi/2 -th*D2R - shoulder - elbow
        res = [-base,-shoulder,-elbow,-wrist]
        #print res
        return map(lambda x:x/D2R,res)

if __name__ == '__main__':
    kin = Kinematics()
    # Grab from GUI
    thetas = [4,74,38,70]
    th = map(lambda x:x*D2R,thetas)
    rbt = kin.get_fkmat(th[0],th[1],th[2],th[3])
    print rbt
    z_in = np.mat([0,0,1,0]).transpose()
    res_vec = np.array(rbt*z_in)
    print res_vec
    dxy = math.sqrt(1 - res_vec[2]*res_vec[2])
    tool_theta = math.atan2(res_vec[2],dxy)
    print tool_theta
    print "\n\n------------------\n\n"
    x = -142.0
    y = -92.0
    z = 50.0
    th = -90.0 # in degrees
    cand = []
    angles = kin.get_invkin(x,y,z,th,0)
    print angles
    if angles and collision_check(angles):
        cand.append(angles)
    angles = kin.get_invkin(x,y,z,th,1)
    print angles
    if angles and collision_check(angles):
        cand.append(angles)
    print cand
    cand = [[43,-20,-39,-26]]
    th = map(lambda x:x*D2R,cand[0])
    rbt = kin.get_fkmat(th[0],th[1],th[2],th[3])
    print rbt
    z_in = np.mat([0,0,1,0]).transpose()
    res_vec = np.array(rbt*z_in)
    #print res_vec
    dxy = math.sqrt(1 - res_vec[2]*res_vec[2])
    tool_theta = math.atan2(res_vec[2],dxy)
    print tool_theta
    