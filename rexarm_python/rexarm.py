import lcm
import math
import time
import numpy as np
import time
import ast
import pickle
from lcmtypes import dynamixel_command_t
from lcmtypes import dynamixel_command_list_t
from lcmtypes import dynamixel_status_t
from lcmtypes import dynamixel_status_list_t
from kinematics import Kinematics
from copy import deepcopy

PI = np.pi
D2R = PI/180.0
R2D = 180.0/3.141592
ANGLE_TOL = 2*PI/180.0 

plan_file = 'waypoints/original_plan.pk'
smooth_plan_file = 'waypoints/smooth_plan.pk'

def clm(x,lb,ub):
    if x > ub:
        return ub
    elif x < lb:
        return lb
    else:
        return x

""" Rexarm Class """
class Rexarm():
    def __init__(self):
        self.use_custom_vel = False
        """ Commanded Values -- B,S,E,W"""
        self.joint_angles = [0.0, 0.0, 0.0, 0.0] # radians
        self.joint_velocities = [0.0,0.0,0.0,0.0] # rad/s
        self.min_angles = map(math.radians,[-180,-118,-118,-120])
        self.max_angles = map(math.radians,[180,120,116,125])
        self.speed = 0.5                         # 0 to 1
        self.max_torque = 0.5                    # 0 to 1

        """ Feedback Values """
        self.joint_angles_fb = [0.0, 0.0, 0.0, 0.0] # radians
        self.speed_fb = [0.0, 0.0, 0.0, 0.0]        # 0 to 1   
        self.load_fb = [0.0, 0.0, 0.0, 0.0]         # -1 to 1  
        self.temp_fb = [0.0, 0.0, 0.0, 0.0]         # Celsius               

        """ Plan - TO BE USED LATER """
        self.plan = []
        self.plan_status = 0
        self.wpt_number = 0
        self.wpt_total = 0
        self.tolerance = 0.03
        self.error = [0.0,0.0,0.0,0.0]

        self.reaching_time = []
        self.feedback_point = [0.0,0.0,0.0,0.0]

        self.kin = Kinematics()

        """ LCM Stuff"""
        self.lc = lcm.LCM()
        lcmMotorSub = self.lc.subscribe("ARM_STATUS",
                                        self.feedback_handler)

    def cmd_publish(self):
        """ 
        Publish the commands to the arm using LCM. 
        NO NEED TO CHANGE.
        You need to activelly call this function to command the arm.
        You can uncomment the print statement to check commanded values.
        """    
        # print "Publishing"
        msg = dynamixel_command_list_t()
        msg.len = 4
        self.clamp()
        for i in range(msg.len):
            cmd = dynamixel_command_t()
            cmd.utime = int(time.time() * 1e6)
            cmd.position_radians = self.joint_angles[i]
            if 0 and self.use_custom_vel:
                cmd.speed = self.joint_velocities[i]
            else:
                cmd.speed = self.speed
            cmd.max_torque = self.max_torque
            #print cmd.position_radians
            msg.commands.append(cmd)
        self.lc.publish("ARM_COMMAND",msg.encode())
    
    def get_feedback(self):
        """
        LCM Handler function
        Called continuously from the GUI 
        NO NEED TO CHANGE
        """
        self.lc.handle_timeout(50)

    def feedback_handler(self, channel, data):
        """
        Feedback Handler for LCM
        NO NEED TO CHANGE FOR NOW.yp
        LATER NEED TO CHANGE TO MANAGE PLAYBACK FUNCTION 
        """
        msg = dynamixel_status_list_t.decode(data)
        for i in range(msg.len):
            self.joint_angles_fb[i] = msg.statuses[i].position_radians
            self.speed_fb[i] = msg.statuses[i].speed 
            self.load_fb[i] = msg.statuses[i].load 
            self.temp_fb[i] = msg.statuses[i].temperature
        
        p = self.joint_angles_fb
        point = self.joint_angles
        self.error = map(abs,[point[i] - p[i] for i in range(len(point))])

        if self.error[0] < self.tolerance and self.error[1] < self.tolerance and self.error[2] < self.tolerance and self.error[3] < self.tolerance:
            self.reaching_time.append([self.wpt_number,time.time()])
            self.feedback_point = p
            self.wpt_number += 1
       

    def clamp(self):
        """
        Clamp Function
        Limit the commanded joint angles to ones physically possible so the 
        arm is not damaged.
        TO DO: IMPLEMENT SUCH FUNCTION
        """
        for i in xrange(len(self.joint_angles)):
            self.joint_angles[i] = clm(self.joint_angles[i],self.min_angles[i],self.max_angles[i])
        

    def plan_command(self):
        """ Command waypoints - TO BE ADDED LATER """

        f = open('waypoints/3_1-1-smooth_wp_and_fb_spline.txt','w')
        f.write("[waypoint number,reaching time], [input waypoints], [feedback waypoints]\n")
        print "In plan_command"

        read_waypoints = False

        if read_waypoints:
            waypoint_file = open('waypoints.txt','r')
            lists_of_waypoints = waypoint_file.readline()
            waypoint_file.close()
            self.plan = ast.literal_eval(lists_of_waypoints)
            # self.plan = pickle.load( open(plan_file, "rb" ) )

        self.speed = 0.5
        self.max_torque = 0.9
        f.write("Speed = " +str(self.speed)+" Torque = "+str(self.max_torque)+"\n")

        if 0 and self.use_custom_vel:
            pos_plan,vel_plan  = self.plan
        else:
            pos_plan = self.plan

        for pidx in range(len(pos_plan)):
            prev_waypoint = self.wpt_number
            if 0 and self.use_custom_vel:
                self.joint_angles = pos_plan[pidx]
                self.joint_velocities = vel_plan[pidx]
            else:
                self.joint_angles = pos_plan[pidx]
            self.cmd_publish()

            while prev_waypoint == self.wpt_number:
                self.get_feedback()
                err = deepcopy(self.error)

            print "Error"
            print err 
            #time.sleep(0.5)

            f.write(str(self.reaching_time[self.wpt_number-1])+", "+str(pos_plan[pidx])+", "+str(self.feedback_point)+"\n")
            print self.wpt_number," :Waypoint executed out of,",self.wpt_total

        f.close()


    def rexarm_FK(self,dh_table, link):
        """
        Calculates forward kinematics for rexarm
        takes a DH table filled with DH parameters of the arm
        and the link to return the position for
        returns a 4-tuple (x, y, z, phi) representing the pose of the 
        desired link
        """
        pass
    	
    def rexarm_IK(self,pose, cfg):
        """
        Calculates inverse kinematics for the rexarm
        pose is a tuple (x, y, z, phi) which describes the desired
        end effector position and orientation.  
        cfg describe elbow down (0) or elbow up (1) configuration
        returns a 4-tuple of joint angles or NONE if configuration is impossible
        """
        angles = self.kin.get_invkin(pose[0],pose[1],pose[2],pose[3],cfg)
        if not angles:
            print "Error- not within reach!"
            return None
        else:
            if self.rexarm_collision_check(angles):
                print angles
                return angles
            else:
                return None
        
    def rexarm_collision_check(q):
        """
        Perform a collision check with the ground and the base
        takes a 4-tuple of joint angles q
        returns true if no collision occurs
        """
        angles = map(lambda x:x*D2R,q)
        if(angles[0]<self.min_angles[0] or angles[0]>self.max_angles[0]):
            return false
        if(angles[1]<self.min_angles[1] or angles[1]>self.max_angles[1]):
            return false
        if(angles[2]<self.min_angles[2] or angles[2]>self.max_angles[2]):
            return false
        if(angles[3]<self.min_angles[3] or angles[3]>self.max_angles[3]):
            return false
        return true 
