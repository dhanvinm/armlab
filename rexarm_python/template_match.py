import cv2
from scipy import signal
import numpy as np
import pdb


def get_locations(img,threshold):
    min_val = np.min(img)
    locs = []
    while(min_val < threshold):
        r,c = np.unravel_index(img.argmin(), img.shape)
        locs.append((r,c))
        print locs
        for i in xrange(-5,5):
            for j in xrange(-5,5):
                img[r+i,c+j] = 255
        min_val = np.min(img)
    return locs

class TemplateMatcher():
	def __init__(self):
		self.ready = False
		self.mask = None
		self.img = None
		self.threshold = 150 # for suppression

	def template_match(self):
		img_rows,img_cols = self.img.shape
		mask_rows,mask_cols = self.mask.shape
		first_col = mask_cols
		first_row = mask_rows
		last_row = img_rows - mask_rows - 1
		last_col = img_cols - mask_cols - 1
		res = np.empty([img_rows,img_cols])
		for i in range(last_row):
			for j in range(last_col):
				# Not sure
				curr = self.img[i:i+mask_rows, j:j+mask_cols]
				res[i,j] = np.mean(np.square(self.mask - curr))

		# print res

		resf = res.astype(np.uint8)
		cv2.namedWindow("RES image",cv2.WINDOW_AUTOSIZE)
		cv2.imshow("RES image",resf)
		cv2.waitKey(0)
		cv2.destroyAllWindows()
		res_min = np.min(resf)
		# resf = np.clip(resf,res_min,12*res_min)
		print res_min
		# resf = np.clip(resf,res_min,res_min+100000)
		# res_max = np.max(resf)
		# normalized = (resf - res_min)/(res_max - res_min)
		# res_img = normalized*255
		# res_img = res_img.astype(np.uint8)
		# cv2.imwrite('heatmap.png',res_img)
		# return get_locations(res_img,self.threshold)

	def cv_template_match(self):
		res_final = np.empty(self.img.shape)
		res_final.fill(255)
		res = cv2.matchTemplate(self.img,self.mask,cv2.TM_CCOEFF_NORMED)
		print res
		threshold = 0.4
		loc = np.where( res >= threshold)
		print loc
		for pt in zip(*loc[::-1]):
			res_final[pt[0],pt[1]] = 0
		return get_locations(res_final,10)
		#    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)



tm = TemplateMatcher()
board = cv2.imread('fulll.png')
board_gray = cv2.cvtColor(board, cv2.COLOR_BGR2GRAY)
tm.img = board_gray

template = cv2.imread('template.png')
temp_gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
tm.mask = temp_gray

res = tm.template_match()
print res