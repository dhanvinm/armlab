import cv2
import pdb
import numpy as np
from PyQt4 import QtGui, QtCore, Qt

def get_locations(img,threshold):
    min_val = np.min(img)
    locs = []
    while(min_val < threshold):
        r,c = np.unravel_index(img.argmin(), img.shape)
        locs.append((c,r))
        #print locs
        for i in xrange(-20,20):
            for j in xrange(-20,20):
                img[r+i,c+j] = 255
        min_val = np.min(img)
    return locs

def get_locations2(img,threshold):
    min_val = np.min(img)
    locs = []
    while(min_val < threshold):
        r,c = np.unravel_index(img.argmin(), img.shape)
        locs.append((r,c))
        #print locs
        for i in xrange(-20,20):
            for j in xrange(-20,20):
                img[r+i,c+j] = 255
        min_val = np.min(img)
    return locs

class TemplateMatcher():
    def __init__(self):
        self.ready = False
        self.mask = None
        self.img = None
        self.threshold = 150 # for suppression

    def template_match(self):
        img_rows,img_cols = self.img.shape
        mask_rows,mask_cols = self.mask.shape
        # new_image = np.reshape(self.img,(img_rows/2,img_cols/2))
        last_row = img_rows - mask_rows
        last_col = img_cols - mask_cols
        res = np.empty([img_rows,img_cols])
        for i in range(last_row):
            for j in range(last_col):
                # Not sure
                curr = self.img[i:i+mask_rows, j:j+mask_cols]
                i_new = i + int(mask_rows/2)
                j_new = j + int(mask_cols/2)
                res[i_new,j_new] = np.mean(np.square(self.mask - curr))

        res_img1 = res.astype(np.uint8)
        cv2.imwrite('res.png',res_img1)

        resf = res.astype(np.float)
        res_min = np.min(resf)
        # resf = np.clip(resf,res_min,12*res_min)
        print res_min
        # resf = np.clip(resf,res_min,res_min+100000)
        res_max = np.max(resf)
        print res_max
        normalized = np.ones((img_rows,img_cols))
        i_new_start = int(mask_rows/2)
        j_new_start = int(mask_cols/2)

        i_new_end = last_row + int(mask_rows/2)
        j_new_end = last_col + int(mask_cols/2)

        for i in range(i_new_start,i_new_end):
            for j in range(j_new_start,j_new_end):
                i_new =  i - int(mask_rows/2)
                j_new = j - int(mask_cols/2)
                normalized[i,j] = (resf[i,j] - res_min)/(res_max - res_min)
                if normalized[i,j] >= 0.5:
                    normalized[i,j] = 1

        res_img = normalized*255
        res_img = res_img.astype(np.uint8)
        cv2.imwrite('heatmap.png',res_img)
        print "Done TMP"
        return get_locations(res_img,self.threshold)

    def cv_template_match(self):
        res_final = np.empty(self.img.shape)
        res_final.fill(255)
        res = cv2.matchTemplate(self.img,self.mask,cv2.TM_CCOEFF_NORMED)
        #print res
        threshold = 0.6
        loc = np.where( res >= threshold)
        #print loc
        for pt in zip(*loc[::-1]):
            res_final[pt[0],pt[1]] = 255- res_final[pt[0],pt[1]]
        return get_locations2(res_final,5)

    def cv_template_match2(self):
        res_final = np.empty(self.img.shape)
        #res_final.fill(255)
        res = cv2.matchTemplate(self.img,self.mask,cv2.TM_CCOEFF_NORMED)
        #print res
        nres = res*255
        nres = nres.astype(np.uint8)
        cv2.imwrite('cv_map.png',nres)
        threshold = 0.6
        loc = np.where( res >= threshold)
        #print loc
        for pt in zip(*loc[::-1]):
            res_final[pt[0],pt[1]] = 255
        #kernel = np.ones((3,3),np.uint8)
        #res_eroded = cv2.erode(res_final,kernel,iterations = 1)
        cv2.imwrite('cv_map_full.png',res_final)
        #res_eroded = res_final
        #res_eroded = res_eroded.astype(np.uint8)
        #circles = cv2.HoughCircles(res_eroded,cv2.HOUGH_GRADIENT,1,20,\
        #            param1=25,param2=8,minRadius=0,maxRadius=8)
        #print "DEBUG\n\n "
        #print circles
        #circles = np.uint16(np.around(circles))
        #cimg = cv2.cvtColor(res_eroded,cv2.COLOR_GRAY2BGR)
        #pts = []
        #for i in circles[0,:]:
        #    # draw the outer circle
        #    cv2.circle(cimg,(i[0],i[1]),i[2],(0,255,0),2)
        #    # draw the center of the circle
        #    cv2.circle(cimg,(i[0],i[1]),2,(0,0,255),1)
        #    pts.append([i[0],i[1]])
        #cv2.imwrite('cv_map_thres.png',cimg)
        locs =  get_locations(res_final,5)
        return locs
        #print "------------------------"
        #print locs
        #self.match_offset =(0.0,0.0)
        #print "-----------------------------"
        #print pts
        #return get_locations(res_final,5)
        #return pts
        #    cv2.rectangle(img_rgb, pt, (pt[0] + w, pt[1] + h), (0,0,255), 2)




class Video():
    def __init__(self,capture):
        self.capture = capture
        self.capture.set(3, 1280)
        self.capture.set(4, 960)
        self.currentFrame=np.array([])
        """ Template stuff"""
        self.setting_template_first = False
        self.setting_template_unconfirmed = False
        self.setting_template_second = False
        self.setting_template_confirmed = False
        self.temp_pt1 = [0.0,0.0]
        self.temp_pt2 = [0.0,0.0]

        self.calibrated = False
        """ 
        Affine Calibration Variables 
        Currently only takes three points: center of arm and two adjacent 
        corners of the base board
        Note that OpenCV requires float32 arrays
        """
        #self.aff_npoints = 8                                   # Change!
        #self.real_coord = np.float32([[302.5,-302.5], [-302.5,-302.5],[-302.5,302.5],[302.5,302.5],[302.5,0.0], [0.0,-302.5],[-302.5,0.0],[0.0,302.5]])
        #self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])   
        self.aff_npoints = 4                                   # Change!
        self.real_coord = np.float32([[302.5,-302.5], [-302.5,-302.5],[-302.5,302.5],[302.5,302.5]])
        self.mouse_coord = np.float32([[0.0, 0.0],[0.0, 0.0],[0.0, 0.0],[0.0, 0.0]])   
        
        self.mouse_click_id = 0;
        self.aff_flag = 0;
        self.aff_matrix = np.float32((2,3))

        """ Template Matcher """
        self.tm = TemplateMatcher()
        self.template_ready = False
        self.template = None
        self.match_offset = None
    
        self.endpoints_set = False
        self.endpoints = []
        self.one_temp = False
    def captureNextFrame(self):
        """                      
        Capture frame, convert to RGB, and return opencv image      
        """
        ret, frame=self.capture.read()
        if(ret==True):
            self.currentFrame=cv2.cvtColor(frame, cv2.COLOR_BAYER_GB2BGR)

    def convertFrame(self):
        """ Converts frame to format suitable for QtGui  """
        try:
            height,width=self.currentFrame.shape[:2]
            if self.calibrated:
                self.currentFrame = cv2.undistort(self.currentFrame, self.camera_matrix, self.dist_coefs, None, self.new_camera_matrix)
            if self.setting_template_confirmed:
                x1 = self.temp_pt1[0]
                y1 = self.temp_pt1[1]
                x2 = self.temp_pt2[0]
                y2 = self.temp_pt2[1]
                self.currentFrame = cv2.rectangle(self.currentFrame, (x1, y1), (x2, y2), (255,0,0), 2)
                crop_img = self.currentFrame[y1:y2, x1:x2]
                crop_gray = cv2.cvtColor(crop_img, cv2.COLOR_BGR2GRAY)
                self.tm.mask = crop_gray
                self.match_offset = crop_gray.shape
                img_gray = cv2.cvtColor(self.currentFrame, cv2.COLOR_BGR2GRAY)
                self.tm.img = img_gray
                #self.template = crop_gray
                self.template_ready = True
                #cv2.imwrite( "template.png", crop_gray)
                #np.save('template.npy',crop_gray)
                self.one_temp = True
                self.setting_template_confirmed = False
            if self.one_temp:
                x1 = self.temp_pt1[0]
                y1 = self.temp_pt1[1]
                x2 = self.temp_pt2[0]
                y2 = self.temp_pt2[1]
                self.currentFrame = cv2.rectangle(self.currentFrame, (x1, y1), (x2, y2), (255,0,0), 2)
            if self.endpoints_set:
                for ep in self.endpoints:
                    #print "Drawing Circle at"
                    #print ep
                    # draw the outer circle
                    #cv2.circle(self.currentFrame,(ep[0],ep[1]),i[2],(0,255,0),2)
                    # draw the center of the circle
                    self.currentFrame = cv2.circle(self.currentFrame,(ep[0]*2,ep[1]*2),5,(0,0,255),-1)
                    
            img=QtGui.QImage(self.currentFrame,
                              width,
                              height,
                              QtGui.QImage.Format_RGB888)
            img=QtGui.QPixmap.fromImage(img)
            self.previousFrame = self.currentFrame
            return img
        except:
            return None

    def loadCalibration(self):
        """
        Load calibration from file and applies to the image
        Look at camera_cal.py final lines to see how that is done
        """
        pass

